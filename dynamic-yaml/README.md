# Dynamic yml
#### This script generates dynamic yml files from a given template it can easily be modified to suit your need.
---

### Requirements
    - Python
---

### Direction
- Create your template replace the fileds to replaced with delimeter - `{}`
    - eg `{MY_CSV}`
- In `generate.py` modify replace_content as per you replace fields.
- In current setup values are passed as `args`.
---

## Authors

* **Atul Kheta** - *AKA phantomhive* - [dynamic-yaml](https://bitbucket.org/phantomhieve/assets/src/master/dynamic-yaml/)
