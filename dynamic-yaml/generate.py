'''
Gerate yaml from template, data provied as arguments.
V1
Arguments
    - jmx file
    - csv file
    eg:
        - python3 generate.py my_jmx.jmx my_csv.csv
'''
import sys
import random


def replace_content(*args, **kwargs):
    content = args[0]
    for key, value in kwargs.items():
        content = content.replace(
            f'{{{key}}}',
            value
        )
    return content

with open('jmeter.template.yml', 'r') as template:
    content = template.read()
    hash_ = random.getrandbits(24)
    for i in range(1, 4):
        temp = replace_content(
            content,
            INPUT_BUCKET = 'some input bucket',
            DYNAMIC_NUM = f"{str(hash_)}-{i}",
            JMX = sys.argv[1],
            CSV = sys.argv[2],
            JTL = f"{sys.argv[1].split('.')[0]}-{i}.jtl",
            LOG = f"jmeter-{sys.argv[1].split('.')[0]}-{i}.log",
            OUTPUT_BUCKET = 'some input bucket',
        )
        with open(f'deploy.jmeter{i}.yml', 'w') as deploy:
            deploy.write(temp) 