# Dynamic yml
#### This is a efficient and modular implementation of [segment tree](https://en.wikipedia.org/wiki/Segment_tree). This implementation is 6 to 10 times faster than recursive implementation and can easily be modified as per your need.
---

### Requirements
    - Python
---

### Direction
- Modify the combine node as per your requirements.
    
- Size of tree could be minimized but for simplicty its kept 3X where x in the size of data points.
- For querying modify `res` with defaults as per your needs.
---

## Authors

* **Atul Kheta** - *AKA phantomhive* - [Efficient Segment tree](https://bitbucket.org/phantomhieve/assets/src/master/dynamic-yaml/)
